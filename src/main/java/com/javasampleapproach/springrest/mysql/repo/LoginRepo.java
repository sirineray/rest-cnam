package com.javasampleapproach.springrest.mysql.repo;

import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.springrest.mysql.model.LoginPk;
import com.javasampleapproach.springrest.mysql.model.login;

public interface LoginRepo extends CrudRepository<login, LoginPk> {

//public login findByLoginPwd(int a ,String pwd );

}
