package com.javasampleapproach.springrest.mysql.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.springrest.mysql.model.LiquidDoss;
import com.javasampleapproach.springrest.mysql.model.LiquidDoss_PK;

public interface LiquidDossRepo extends CrudRepository<LiquidDoss, LiquidDoss_PK> 
 {

	 @Query(value= "SELECT MAX(numdos)+1 FROM LiquidDoss  WHERE  annee =?1 and  code_br=?2 ", nativeQuery= true)
	   public int  getMaxRef( int anne , int br  );
	}


	

