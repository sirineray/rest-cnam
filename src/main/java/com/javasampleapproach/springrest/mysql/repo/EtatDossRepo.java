package com.javasampleapproach.springrest.mysql.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.springrest.mysql.model.EtatDoss;
import com.javasampleapproach.springrest.mysql.model.EtatDossPk;

public interface EtatDossRepo extends CrudRepository<EtatDoss, EtatDossPk> {
	 @Query(value= "SELECT MAX(sit_code) FROM etat_doss  WHERE  annee_doss =?1 and  code_br=?2 and num_doss=?3", nativeQuery= true)
	   public int  getMaxSituation( int anne , int br , int num   );

	 @Query(value= "SELECT *FROM etat_doss WHERE code_br=?1 and sit_code=?2 and date_fin = '' ", nativeQuery= true)
	 // WHERE code_br=?1 and sit_code=?2 and date_fin	is null
	   public List<EtatDoss>  getListeDossier( int br ,int sit   );
	 
	
}
