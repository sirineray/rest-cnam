package com.javasampleapproach.springrest.mysql.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.springrest.mysql.model.huissier;
import com.javasampleapproach.springrest.mysql.model.huissier_pk;

public interface HuisRepo extends CrudRepository<huissier, huissier_pk> {

	@Modifying
	@Query(value="delete from huissier where code= :code",nativeQuery=true)
	int deleteHuissier(int code);

}
