package com.javasampleapproach.springrest.mysql.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.javasampleapproach.springrest.mysql.model.tribunal;

@Repository
public interface tribunalRepo extends JpaRepository<tribunal, Long> {

	@Modifying
	@Query(value="delete from tribunal where code= :code",nativeQuery=true)
	int deleteTribunal(int code);

}
