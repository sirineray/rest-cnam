package com.javasampleapproach.springrest.mysql.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.javasampleapproach.springrest.mysql.model.DossierJugement;
import com.javasampleapproach.springrest.mysql.model.EtatDoss;
import com.javasampleapproach.springrest.mysql.model.Jugement_PK;

public interface DossjugRepo extends CrudRepository<DossierJugement, Jugement_PK> {

 @Query(value= "SELECT MAX(numdos)+1 FROM dossierjugement  WHERE  annee =?1 and  code_br=?2 ", nativeQuery= true)
   public int  getMaxRef( int a , int b  );
// d WHERE d.annee =?1 and d.code_br=?2
 
 
 @Query(value= "SELECT  cod_br ,annee, numdos FROM etat_doss WHERE matricule_assure=?1 and cle=?2 and"
 		+ " (cod_br ,annee, numdos ) in (select cod_br ,annee, numdos from etat_doss where sit_code=?c and date_fin is null) ", nativeQuery= true)
 // WHERE code_br=?1 and sit_code=?2 and date_fin	is null
   public List<EtatDoss>  getInstance( int a ,int b ,int c   );
}

