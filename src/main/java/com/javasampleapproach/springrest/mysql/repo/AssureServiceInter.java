package com.javasampleapproach.springrest.mysql.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.javasampleapproach.springrest.mysql.model.Ass;
import com.javasampleapproach.springrest.mysql.model.AssCle;


public interface AssureServiceInter extends CrudRepository <Ass, AssCle>{


	public List<Ass> findByNom(String nom);
	 
}
