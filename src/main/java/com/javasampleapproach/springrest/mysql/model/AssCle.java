package com.javasampleapproach.springrest.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.springframework.context.annotation.EnableMBeanExport;

 
@Embeddable
public class AssCle implements Serializable {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public AssCle() {}
	
	public AssCle(int matricule, int cle) {
		super();
		this.matricule = matricule;
		this.cle = cle;
	}
	
	@Column(name = "matricule")
	private int  matricule;
 
	
	@Column(name = "cle")
	private int  cle ;
	
	public int getMatricule() {
		return matricule;
	}

	public void setMatricule(int matricule) {
		this.matricule = matricule;
	}

	public int getCle() {
		return cle;
	}

	public void setCle(int cle) {
		this.cle = cle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cle;
		result = prime * result + matricule;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssCle other = (AssCle) obj;
		if (cle != other.cle)
			return false;
		if (matricule != other.matricule)
			return false;
		return true;
	}



}
