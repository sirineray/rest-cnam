package com.javasampleapproach.springrest.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;




@Embeddable	
public class huissier_pk implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	

public huissier_pk() {}
 	
@Column(name = "code")
private int  code;


@Column(name = "cle")
private int  cle ;

	public huissier_pk (int code, int cle) {

		this.code= code;
		this.cle = cle;
	}
	




	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getCle() {
		return cle;
	}

	public void setCle(int cle) {
		this.cle = cle;
	}





	@Override
	public String toString() {
		return "huissier_pk [code=" + code + ", cle=" + cle + "]";
	}
	
	

}
