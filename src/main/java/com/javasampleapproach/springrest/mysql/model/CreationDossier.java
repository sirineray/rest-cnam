package com.javasampleapproach.springrest.mysql.model;

public class CreationDossier {
	int  huis_code; 
	int  huis_cle;
	int  trub;
	int cle;
	int matr  ; 
  String date_accident ;
  int heure  ; 
  String  actjug ;
   String  date_juge_string ;
   int N_jug ;
   int empMat  ;
   int empCle ;
   
   
public CreationDossier() {
	 
	// TODO Auto-generated constructor stub
}


public CreationDossier(int huis_code, int huis_cle, int trub, int cle, int matr, String date_accident, int heure,
		String actjug, String date_juge_string, int n_jug, int empMat, int empCle) {
	super();
	this.huis_code = huis_code;
	this.huis_cle = huis_cle;
	this.trub = trub;
	this.cle = cle;
	this.matr = matr;
	this.date_accident = date_accident;
	this.heure = heure;
	this.actjug = actjug;
	this.date_juge_string = date_juge_string;
	this.N_jug = n_jug;
	this.empMat = empMat;
	this.empCle = empCle;
}


public int getHuis_code() {
	return huis_code;
}


public void setHuis_code(int huis_code) {
	this.huis_code = huis_code;
}


public int getHuis_cle() {
	return huis_cle;
}


public void setHuis_cle(int huis_cle) {
	this.huis_cle = huis_cle;
}


public int getTrub() {
	return trub;
}


public void setTrub(int trub) {
	this.trub = trub;
}


public int getCle() {
	return cle;
}


public void setCle(int cle) {
	this.cle = cle;
}


public int getMatr() {
	return matr;
}


public void setMatr(int matr) {
	this.matr = matr;
}


public String getDate_accident() {
	return date_accident;
}


public void setDate_accident(String date_accident) {
	this.date_accident = date_accident;
}


public int getHeure() {
	return heure;
}


public void setHeure(int heure) {
	this.heure = heure;
}


public String getActjug() {
	return actjug;
}


public void setActjug(String actjug) {
	this.actjug = actjug;
}


public String getDate_juge_string() {
	return date_juge_string;
}


public void setDate_juge_string(String date_juge_string) {
	this.date_juge_string = date_juge_string;
}


public int getN_jug() {
	return N_jug;
}


public void setN_jug(int n_jug) {
	N_jug = n_jug;
}


public int getEmpMat() {
	return empMat;
}


public void setEmpMat(int empMat) {
	this.empMat = empMat;
}


public int getEmpCle() {
	return empCle;
}


public void setEmpCle(int empCle) {
	this.empCle = empCle;
}
   
   
}
