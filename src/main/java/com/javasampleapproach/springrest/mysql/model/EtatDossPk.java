package com.javasampleapproach.springrest.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable	
public class EtatDossPk implements Serializable{

	private static final long serialVersionUID = 1L;

	
	
	@Column(name = "code_br")
	int code_br ; 

	@Column(name = "num_doss")
	int num_doss ; 
	@Column(name = "annee_doss")
	int annee_doss ; 
	@Column(name = "sit_code")
	int sit_code ;
	
	public EtatDossPk() {
	 
		// TODO Auto-generated constructor stub
	}

	public EtatDossPk(int code_br, int num_doss, int annee_doss, int sit_code) {
		super();
		this.code_br = code_br;
		this.num_doss = num_doss;
		this.annee_doss = annee_doss;
		this.sit_code = sit_code;
	}

	public int getCode_br() {
		return code_br;
	}

	public void setCode_br(int code_br) {
		this.code_br = code_br;
	}

	public int getNum_doss() {
		return num_doss;
	}

	public void setNum_doss(int num_doss) {
		this.num_doss = num_doss;
	}

	public int getAnnee_doss() {
		return annee_doss;
	}

	public void setAnnee_doss(int annee_doss) {
		this.annee_doss = annee_doss;
	}

	public int getSit_code() {
		return sit_code;
	}

	public void setSit_code(int sit_code) {
		this.sit_code = sit_code;
	}

	
	
}