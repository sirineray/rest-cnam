package com.javasampleapproach.springrest.mysql.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
@Entity

public class EtatDoss {
@EmbeddedId
	EtatDossPk  etat_doss_pk;
@Column(name = "date_deb")  
String date_deb   ; 
@Column(name = "date_fin")
String  date_fin ;
@Column(name = "adress_ip")
	String adress_ip; 
@Column(name = "mat_agent")
	int mat_agent ;

	public EtatDossPk getEtat_doss_pk() {
		return etat_doss_pk;
	}
	public void setEtat_doss_pk(EtatDossPk etat_doss_pk) {
		this.etat_doss_pk = etat_doss_pk;
	}
	public String getDate_deb() {
		return date_deb;
	}
	public void setDate_deb(String date_deb) {
		this.date_deb = date_deb;
	}
	public String getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(String date_fin) {
		this.date_fin = date_fin;
	}
	public String getAdress_ip() {
		return adress_ip;
	}
	public void setAdress_ip(String adress_ip) {
		this.adress_ip = adress_ip;
	}
	public int getMat_agent() {
		return mat_agent;
	}
	public void setMat_agent(int mat_agent) {
		this.mat_agent = mat_agent;
	}
	public EtatDoss(EtatDossPk etat_doss_pk, String date_deb, String date_fin, String adress_ip, int mat_agent) {
		super();
		this.etat_doss_pk = etat_doss_pk;
		this.date_deb = date_deb;
		this.date_fin = date_fin;
		this.adress_ip = adress_ip;
		this.mat_agent = mat_agent;
	
	}
	public EtatDoss() {
		
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "EtatDoss etat_doss_pk=" + etat_doss_pk + ", date_deb=" + date_deb + ", date_fin=" + date_fin
				+ ", adress_ip=" + adress_ip + ", mat_agent=" + mat_agent ;
	}

	
	
}



	
