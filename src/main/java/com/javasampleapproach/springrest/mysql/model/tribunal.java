package com.javasampleapproach.springrest.mysql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tribunal")





public class tribunal {

	public tribunal() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Column(name = "libelle")
	private String  libelle;
	
	@Id
	@Column(name = "code")
	private int code ;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "tribunal [libelle=" + libelle + ", code=" + code + "]";
	}
	
	
	
}
