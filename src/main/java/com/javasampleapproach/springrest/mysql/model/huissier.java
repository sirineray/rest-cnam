package com.javasampleapproach.springrest.mysql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="huissier")

public class huissier {
	

	@Id
	@GeneratedValue
	private Long id;
		
	@Column(name = "Nom")
	private String  nom ;
	
	@Column(name = "prenom")
	private String prenom ;
	
	@Column(name = "adrsee")
	private String adresse  ;
	@Column(name = "tel")
	private int tel ;
	@Column(name = "rib")
	private String  rib ;
	@Column(name = "fax")
	private int fax ;
	
	@Column(name = "eMail")
	private String eMail ;
	@Column(name = "numPat")
	private int numPat  ;

	
	@Column(name = "code",unique=true)
	private int  code;


	@Column(name = "cle",unique=true)
	private int  cle ;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}

	public int getFax() {
		return fax;
	}

	public void setFax(int fax) {
		this.fax = fax;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public int getNumPat() {
		return numPat;
	}

	public void setNumPat(int numPat) {
		this.numPat = numPat;
	}

	public huissier() {
		
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getCle() {
		return cle;
	}

	public void setCle(int cle) {
		this.cle = cle;
	}

	@Override
	public String toString() {
		return "huissier [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse + ", tel=" + tel
				+ ", rib=" + rib + ", fax=" + fax + ", eMail=" + eMail + ", numPat=" + numPat + ", code=" + code
				+ ", cle=" + cle + "]";
	}

	

	

}
