package com.javasampleapproach.springrest.mysql.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table (name="dossierjugement")

public class DossierJugement {

@EmbeddedId

public Jugement_PK Jugement_PK;

@Column(name = "code")
private int  code ;

@Column(name = "cle")
private int cle ;

@Column(name = "Tri_code")
private int Tri_code  ;

@Column(name = "matricule")
private int matricule ;
@Column(name = "matriculeAssure")
private int matriculeAssure ;
@Column(name = "dateAt")
private String dateAt ;
@Column(name = "heureAt")
private int heureAt ;

@Column(name = "actJugement")
private String actJugement ;
@Column(name = "dateJugement")
private String  dateJugement ;
@Column(name = "numJugement")
private int numJugement ;
@Column(name = "empCode")
private int empCode ;

@Column(name = "empCle")
private int empCle ;

public Jugement_PK getJugement_PK() {
	return Jugement_PK;
}

public void setJugement_PK(Jugement_PK jugement_PK) {
	Jugement_PK = jugement_PK;
}

public int getCode() {
	return code;
}

public void setCode(int code) {
	this.code = code;
}

public int getCle() {
	return cle;
}

public void setCle(int cle) {
	this.cle = cle;
}

public int getTri_code() {
	return Tri_code;
}

public void setTri_code(int tri_code) {
	Tri_code = tri_code;
}

public int getMatricule() {
	return matricule;
}

public void setMatricule(int matricule) {
	this.matricule = matricule;
}

public int getMatriculeAssure() {
	return matriculeAssure;
}

public void setMatriculeAssure(int matriculeAssure) {
	this.matriculeAssure = matriculeAssure;
}

public String getDateAt() {
	return dateAt;
}

public void setDateAt(String dateAt) {
	this.dateAt = dateAt;
}

public int getHeureAt() {
	return heureAt;
}

public void setHeureAt(int heureAt) {
	this.heureAt = heureAt;
}

public String getActJugement() {
	return actJugement;
}

public void setActJugement(String actJugement) {
	this.actJugement = actJugement;
}

public String getDateJugement() {
	return dateJugement;
}

public void setDateJugement(String dateJugement) {
	this.dateJugement = dateJugement;
}

public int getNumJugement() {
	return numJugement;
}

public void setNumJugement(int numJugement) {
	this.numJugement = numJugement;
}

public int getEmpCode() {
	return empCode;
}

public void setEmpCode(int empCode) {
	this.empCode = empCode;
}

public int getEmpCle() {
	return empCle;
}

public void setEmpCle(int empCle) {
	this.empCle = empCle;
}

public DossierJugement(com.javasampleapproach.springrest.mysql.model.Jugement_PK jugement_PK, int code, int cle,
		int tri_code, int matricule, int matriculeAssure, String dateAt, int heureAt, String actJugement,
		String dateJugement, int numJugement, int empCode, int empCle) {
	super();
	Jugement_PK = jugement_PK;
	this.code = code;
	this.cle = cle;
	Tri_code = tri_code;
	this.matricule = matricule;
	this.matriculeAssure = matriculeAssure;
	this.dateAt = dateAt;
	this.heureAt = heureAt;
	this.actJugement = actJugement;
	this.dateJugement = dateJugement;
	this.numJugement = numJugement;
	this.empCode = empCode;
	this.empCle = empCle;
}

public DossierJugement() {
	
	// TODO Auto-generated constructor stub
}

@Override
public String toString() {
	return "DossierJugement [Jugement_PK=" + Jugement_PK + ", code=" + code + ", cle=" + cle + ", Tri_code=" + Tri_code
			+ ", matricule=" + matricule + ", matriculeAssure=" + matriculeAssure + ", dateAt=" + dateAt + ", heureAt="
			+ heureAt + ", actJugement=" + actJugement + ", dateJugement=" + dateJugement + ", numJugement="
			+ numJugement + ", empCode=" + empCode + ", empCle=" + empCle + "]";
}




	
}
