package com.javasampleapproach.springrest.mysql.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "LiquidDoss")

public class LiquidDoss {
	@EmbeddedId
	public LiquidDoss_PK liqui_pk;
	
	@Column(name = "codeBr")
	private Integer codeBr  ;
	
	@Column(name = "numDoss")
	private Integer numDoss ;
	
	@Column(name = "anneDoss")
	private Integer anneDoss  ;
	@Column(name = "numOp")
	private Integer numOp ;
	@Column(name = "annOp")
	private Integer  annOp ;
	@Column(name = "brgOp")
	private Integer brgOp ;
	
	@Column(name = "DateLiquidation")
	private String DateLiquidation ;
	@Column(name = "TauxRente")
	private Integer  TauxRente ;
	

	@Column(name = "SalaireSaisie")
	private  Integer SalaireSaisie ;
	@Column(name = "SalaireRetenu")
	private Integer SalaireRetenu ;
	
	@Column(name = "TauxIpp")
	private Integer TauxIpp ;
	@Column(name = "RenteAnnuelle")
	private float  RenteAnnuelle ;
	

	@Column(name = "FrequencePaiement")
	private String FrequencePaiement ;
	
	@Column(name = "typePaiement")
	private String  typePaiement;

	public LiquidDoss_PK getLiqui_pk() {
		return liqui_pk;
	}

	public void setLiqui_pk(LiquidDoss_PK liqui_pk) {
		this.liqui_pk = liqui_pk;
	}

	public Integer getCodeBr() {
		return codeBr;
	}

	public void setCodeBr(Integer codeBr) {
		this.codeBr = codeBr;
	}

	public Integer getNumDoss() {
		return numDoss;
	}

	public void setNumDoss(Integer numDoss) {
		this.numDoss = numDoss;
	}

	public Integer getAnneDoss() {
		return anneDoss;
	}

	public void setAnneDoss(Integer anneDoss) {
		this.anneDoss = anneDoss;
	}

	public Integer getNumOp() {
		return numOp;
	}

	public void setNumOp(Integer numOp) {
		this.numOp = numOp;
	}

	public Integer getAnnOp() {
		return annOp;
	}

	public void setAnnOp(Integer annOp) {
		this.annOp = annOp;
	}

	public Integer getBrgOp() {
		return brgOp;
	}

	public void setBrgOp(Integer brgOp) {
		this.brgOp = brgOp;
	}

	public String getDateLiquidation() {
		return DateLiquidation;
	}

	public void setDateLiquidation(String dateLiquidation) {
		DateLiquidation = dateLiquidation;
	}

	public Integer getTauxRente() {
		return TauxRente;
	}

	public void setTauxRente(Integer tauxRente) {
		TauxRente = tauxRente;
	}

	public Integer getSalaireSaisie() {
		return SalaireSaisie;
	}

	public void setSalaireSaisie(Integer salaireSaisie) {
		SalaireSaisie = salaireSaisie;
	}

	public Integer getSalaireRetenu() {
		return SalaireRetenu;
	}

	public void setSalaireRetenu(Integer salaireRetenu) {
		SalaireRetenu = salaireRetenu;
	}

	public Integer getTauxIpp() {
		return TauxIpp;
	}

	public void setTauxIpp(Integer tauxIpp) {
		TauxIpp = tauxIpp;
	}

	public float getRenteAnnuelle() {
		return RenteAnnuelle;
	}

	public void setRenteAnnuelle(float renteAnnuelle) {
		RenteAnnuelle = renteAnnuelle;
	}

	public String getFrequencePaiement() {
		return FrequencePaiement;
	}

	public void setFrequencePaiement(String frequencePaiement) {
		FrequencePaiement = frequencePaiement;
	}

	public String getTypePaiement() {
		return typePaiement;
	}

	public void setTypePaiement(String typePaiement) {
		this.typePaiement = typePaiement;
	}

	public LiquidDoss(LiquidDoss_PK liqui_pk, Integer codeBr, Integer numDoss, Integer anneDoss, Integer numOp,
			Integer annOp, Integer brgOp, String dateLiquidation, Integer tauxRente, Integer salaireSaisie,
			Integer salaireRetenu, Integer tauxIpp, float renteAnnuelle, String frequencePaiement,
			String typePaiement) {
		super();
		this.liqui_pk = liqui_pk;
		this.codeBr = codeBr;
		this.numDoss = numDoss;
		this.anneDoss = anneDoss;
		this.numOp = numOp;
		this.annOp = annOp;
		this.brgOp = brgOp;
		DateLiquidation = dateLiquidation;
		TauxRente = tauxRente;
		SalaireSaisie = salaireSaisie;
		SalaireRetenu = salaireRetenu;
		TauxIpp = tauxIpp;
		RenteAnnuelle = renteAnnuelle;
		FrequencePaiement = frequencePaiement;
		this.typePaiement = typePaiement;
	}

	public LiquidDoss() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "LiquidDoss [liqui_pk=" + liqui_pk + ", codeBr=" + codeBr + ", numDoss=" + numDoss + ", anneDoss="
				+ anneDoss + ", numOp=" + numOp + ", annOp=" + annOp + ", brgOp=" + brgOp + ", DateLiquidation="
				+ DateLiquidation + ", TauxRente=" + TauxRente + ", SalaireSaisie=" + SalaireSaisie + ", SalaireRetenu="
				+ SalaireRetenu + ", TauxIpp=" + TauxIpp + ", RenteAnnuelle=" + RenteAnnuelle + ", FrequencePaiement="
				+ FrequencePaiement + ", typePaiement=" + typePaiement + "]";
	}

}