package com.javasampleapproach.springrest.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable	
public class LiquidDoss_PK implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(name ="idLiquidation")
	private Integer  idLiquidation;


	@Column(name = "anneLiq")
	private Integer  anneLiq ;


	public Integer getIdLiquidation() {
		return idLiquidation;
	}


	public void setIdLiquidation(Integer idLiquidation) {
		this.idLiquidation = idLiquidation;
	}


	public Integer getAnneLiq() {
		return anneLiq;
	}


	public void setAnneLiq(Integer anneLiq) {
		this.anneLiq = anneLiq;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public LiquidDoss_PK(Integer idLiquidation, Integer anneLiq) {
		super();
		this.idLiquidation = idLiquidation;
		this.anneLiq = anneLiq;
	}


	public LiquidDoss_PK() {
		// TODO Auto-generated constructor stub
	}


}