package com.javasampleapproach.springrest.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

@Embeddable
public class LoginPk implements Serializable  {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "login" )
	private int   login;
 
	@Column(name = "pwd")
	private String   pwd ;
	
	public LoginPk() {
		 
		// TODO Auto-generated constructor stub
	}


 

	public LoginPk(int login, String pwd) {
	 
		this.login = login;
		this.pwd = pwd;
	}




	public int getLogin() {
		return login;
	}


	public void setLogin(int  login) {
		this.login = login;
	}


	public String getPwd() {
		return pwd;
	}


	public void setPwd(String pwd) {
		this.pwd = pwd;
	}


	
	
}
