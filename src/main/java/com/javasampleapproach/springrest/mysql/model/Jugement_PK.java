package com.javasampleapproach.springrest.mysql.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;




@Embeddable	
public class Jugement_PK implements Serializable{
	
	

	private static final long serialVersionUID = 1L;
		
		

	public Jugement_PK() {}

	@Column(name = "codeBr")
	private int  codeBr;


	@Column(name = "numdos")
	private int  numDoss ;

	@Column(name = "annee")
	private int  anneDoss ;

	
	
	public Jugement_PK (int codeBr, int numDoss , int anneDoss ) {

			this.codeBr= codeBr;
			this.numDoss = numDoss;
			this.anneDoss = anneDoss;
		}



	public int getCodeBr() {
		return codeBr;
	}



	public void setCodeBr(int codeBr) {
		this.codeBr = codeBr;
	}



	public int getNumDoss() {
		return numDoss;
	}



	public void setNumDoss(int numDoss) {
		this.numDoss = numDoss;
	}



	public int getAnneDoss() {
		return anneDoss;
	}



	public void setAnneDoss(int anneDoss) {
		this.anneDoss = anneDoss;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

 


	

	
	
	
	
	
}

