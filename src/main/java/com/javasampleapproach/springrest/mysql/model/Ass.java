package com.javasampleapproach.springrest.mysql.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.springframework.data.annotation.Id;


	@Entity
	@Table(name = "Ass")
    
	public class Ass {
 
@EmbeddedId
      public  AssCle ass_pk;

		@Column(name = "Nom")
		private String  nom ;
		
		@Column(name = "prenom")
		private String prenom ;
		@Column(name = "Datenaiss")
		private String Datenaiss ;
		@Column(name = "adrsee")
		private String adresse  ;
		@Column(name = "NumeroCin")
		private int NumeroCin;
		@Column(name = "Nationalite")
		private String Nationalite ;
		@Column(name = "Etat")
		private String  Etat ;
		@Column(name = "lieu")
		private String lieu ;
		@Column(name = "Datelivraison")
		private String Datelivraison ;
		@Column(name = "Pays")
		private String Pays  ;
		@Column(name = "Localite")
		private String Localite;
		@Column(name = "cite")
		private String cite ;
		@Column(name = "CodePostale")
		private int CodePostale ;
		@Column(name = "EmpMat")
		private int EmpMat;
		@Column(name = "EmpCle")
		private int EmpCle ;
		@Column(name = "Type_Declaration")
		private String Type_Declaration ;
		@Column(name = "Code_Com_Med")
		private String Code_Com_Med ;
		@Column(name = "Date_Accident")
		private String  Date_Accident ;
		@Column(name = "Heure")
		private String heure ;
		@Column(name = "Date_Saisie_Dos")
		private String Date_Saisie_Dos ;
		
		
		
		
		
		public AssCle getAss_pk() {
			return ass_pk;
		}
		public void setAss_pk(AssCle ass_pk) {
			this.ass_pk = ass_pk;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		public String getDatenaiss() {
			return Datenaiss;
		}
		public void setDatenaiss(String datenaiss) {
			Datenaiss = datenaiss;
		}
		public String getAdresse() {
			return adresse;
		}
		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		public int getNumeroCin() {
			return NumeroCin;
		}
		public void setNumeroCin(int numeroCin) {
			NumeroCin = numeroCin;
		}
		public String getNationalite() {
			return Nationalite;
		}
		public void setNationalite(String nationalite) {
			Nationalite = nationalite;
		}
		public String getEtat() {
			return Etat;
		}
		public void setEtat(String etat) {
			Etat = etat;
		}
		public String getLieu() {
			return lieu;
		}
		public void setLieu(String lieu) {
			this.lieu = lieu;
		}
		public String getDatelivraison() {
			return Datelivraison;
		}
		public void setDatelivraison(String datelivraison) {
			Datelivraison = datelivraison;
		}
		public String getPays() {
			return Pays;
		}
		public void setPays(String pays) {
			Pays = pays;
		}
		public String getLocalite() {
			return Localite;
		}
		public void setLocalite(String localite) {
			Localite = localite;
		}
		public String getCite() {
			return cite;
		}
		public void setCite(String cite) {
			this.cite = cite;
		}
		public int getCodePostale() {
			return CodePostale;
		}
		public void setCodePostale(int codePostale) {
			CodePostale = codePostale;
		}
		public int getEmpMat() {
			return EmpMat;
		}
		public void setEmpMat(int empMat) {
			EmpMat = empMat;
		}
		public int getEmpCle() {
			return EmpCle;
		}
		public void setEmpCle(int empCle) {
			EmpCle = empCle;
		}
		
		
		public String getType_Declaration() {
			return Type_Declaration;
		}
		public void setType_Declaration(String type_Declaration) {
			Type_Declaration = type_Declaration;
		}
		public String getCode_Com_Med() {
			return Code_Com_Med;
		}
		public void setCode_Com_Med(String code_Com_Med) {
			Code_Com_Med = code_Com_Med;
		}
		public String getDate_Accident() {
			return Date_Accident;
		}
		public void setDate_Accident(String date_Accident) {
			Date_Accident = date_Accident;
		}
		public String getHeure() {
			return heure;
		}
		public void setHeure(String heure) {
			this.heure = heure;
		}
		public String getDate_Saisie_Dos() {
			return Date_Saisie_Dos;
		}
		public void setDate_Saisie_Dos(String date_Saisie_Dos) {
			Date_Saisie_Dos = date_Saisie_Dos;
		}
		
		public Ass(AssCle ass_pk, String nom, String prenom, String datenaiss, String adresse, int numeroCin,
				String nationalite, String etat, String lieu, String datelivraison, String pays, String localite,
				String cite, int codePostale, int empMat, int empCle, String type_Declaration, String code_Com_Med,
				String date_Accident, String heure, String date_Saisie_Dos) {
			super();
			this.ass_pk = ass_pk;
			this.nom = nom;
			this.prenom = prenom;
			Datenaiss = datenaiss;
			this.adresse = adresse;
			NumeroCin = numeroCin;
			Nationalite = nationalite;
			Etat = etat;
			this.lieu = lieu;
			Datelivraison = datelivraison;
			Pays = pays;
			Localite = localite;
			this.cite = cite;
			CodePostale = codePostale;
			EmpMat = empMat;
			EmpCle = empCle;
			Type_Declaration = type_Declaration;
			Code_Com_Med = code_Com_Med;
			Date_Accident = date_Accident;
			this.heure = heure;
			Date_Saisie_Dos = date_Saisie_Dos;
		}
		public Ass() {
			
			// TODO Auto-generated constructor stub
		}
		@Override
		public String toString() {
			return "Ass [ass_pk=" + ass_pk + ", nom=" + nom + ", prenom=" + prenom + ", Datenaiss=" + Datenaiss
					+ ", adresse=" + adresse + ", NumeroCin=" + NumeroCin + ", Nationalite=" + Nationalite + ", Etat="
					+ Etat + ", lieu=" + lieu + ", Datelivraison=" + Datelivraison + ", Pays=" + Pays + ", Localite="
					+ Localite + ", cite=" + cite + ", CodePostale=" + CodePostale + ", EmpMat=" + EmpMat + ", EmpCle="
					+ EmpCle + ", Type_Declaration=" + Type_Declaration + ", Code_Com_Med=" + Code_Com_Med
					+ ", Date_Accident=" + Date_Accident + ", heure=" + heure + ", Date_Saisie_Dos=" + Date_Saisie_Dos
					+ "]";
		}
		
		
		
	
		
		
		
	}

	
	


