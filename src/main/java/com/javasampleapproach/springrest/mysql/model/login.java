package com.javasampleapproach.springrest.mysql.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
 


@Entity
@Table(name = "login")

public class login {

	@EmbeddedId
    public  LoginPk login_pk;

	public login() {
		 
		// TODO Auto-generated constructor stub
	}

	@Column(name = "username")
	private String  username ;

	public LoginPk getLogin_pk() {
		return login_pk;
	}

	public void setLogin_pk(LoginPk login_pk) {
		this.login_pk = login_pk;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public login(LoginPk login_pk, String username) {
		super();
		this.login_pk = login_pk;
		this.username = username;
	}

	@Override
	public String toString() {
		return "login [login_pk=" + login_pk + ", username=" + username + "]";
	}
	

//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	private long id;

	
 
 
	
	
}
