package com.javasampleapproach.springrest.mysql.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.springrest.mysql.model.EtatDoss;
import com.javasampleapproach.springrest.mysql.model.EtatDossPk;
import com.javasampleapproach.springrest.mysql.repo.EtatDossRepo;
 

@Service
public class EtatDoss_service {

	@Autowired
	EtatDossRepo  doss_repo  ;
	
	public void create_etat(EtatDossPk pk , 
			String Date_deb  , String Date_fin  , int mat_agent, String adress_ip )
	{
	EtatDoss dossier = new EtatDoss(pk, Date_deb, Date_fin, adress_ip, mat_agent);
		
		doss_repo.save(dossier );
		
	}
	
	public   Optional<EtatDoss> getSituation(EtatDossPk doss_pk  )
	{
	 
		
		return doss_repo.findById(doss_pk);
		
}
	
	public int getMaxsit ( int anne , int br , int num )
	{
		
		return doss_repo.getMaxSituation(anne, br, num) ;
	}
	
	public   List <EtatDoss> instance(int br , int sit )

	{
		
		 return doss_repo.getListeDossier(br, sit);
	}
	
}
