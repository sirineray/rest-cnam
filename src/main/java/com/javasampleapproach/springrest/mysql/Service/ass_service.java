package com.javasampleapproach.springrest.mysql.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.springrest.mysql.model.Ass;
import com.javasampleapproach.springrest.mysql.model.AssCle;
import com.javasampleapproach.springrest.mysql.repo.AssureServiceInter;

@Service
public class ass_service {
@Autowired
AssureServiceInter repo;

public Iterable<Ass> getAll()
{
return repo.findAll();
}
public List<Ass> getByNom(String nom)
{
	return repo.findByNom(nom);
}

public  Optional<Ass> getByPk(AssCle ass_pk)
{
	return repo.findById(ass_pk);
}
 

 
}
