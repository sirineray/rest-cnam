package com.javasampleapproach.springrest.mysql.Service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.springrest.mysql.model.tribunal;
import com.javasampleapproach.springrest.mysql.repo.tribunalRepo;

@Service
public class TribunalService {

	
	@Autowired
	tribunalRepo tribunalRepo;
	
	public void addTribunal(tribunal tribunal) {
		
		tribunalRepo.save(tribunal);
	}
	
	@Transactional
	public void deleteTriunal(int code) {
		
		tribunalRepo.deleteTribunal(code);
	}
}
