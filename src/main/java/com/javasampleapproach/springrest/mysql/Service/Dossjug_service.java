package com.javasampleapproach.springrest.mysql.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.javasampleapproach.springrest.mysql.model.DossierJugement;
import com.javasampleapproach.springrest.mysql.model.EtatDossPk;
import com.javasampleapproach.springrest.mysql.model.Jugement_PK;
import com.javasampleapproach.springrest.mysql.model.huissier;
import com.javasampleapproach.springrest.mysql.model.huissier_pk;
import com.javasampleapproach.springrest.mysql.repo.DossjugRepo;
import com.javasampleapproach.springrest.mysql.repo.HuisRepo;

@Service
public class Dossjug_service {
 
	
	@Autowired
	EtatDoss_service servivce_etat ; 
	
	
	@Autowired
	 DossjugRepo DossjugRepo  ;
	
	
	public Optional<DossierJugement> getDossjug(Jugement_PK pk)
	{
		
		return DossjugRepo.findById(pk);		
		
	}
	
	
	public int getAll(int a , int b )
	{
		
		return 
				DossjugRepo.getMaxRef(a ,b);		
		
	}
	
	public void SaveDossier( int num_dossier , int code, int cle, int tri_code, int matricule, int matriculeAssure, String dateAt,
			int heureAt, String actJugement, String dateJugement, int numJugement,
			int empCode, int empCle)
	{

		DateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
		
		Date date = new Date();
		String dateformat = sdf.format(date);
		
	  Jugement_PK  doss_pk = new Jugement_PK(99,num_dossier,2019) ;
	  
	  DossierJugement doss_jug = new DossierJugement(doss_pk, code, cle, tri_code, matricule,
			  matriculeAssure, dateAt, heureAt, actJugement,
			  dateJugement, numJugement, empCode, empCle) ;
	  
	        DossjugRepo.save(doss_jug);
	        
	        EtatDossPk  pk_etat = new EtatDossPk(99, num_dossier, 2019, 1);
	        
	        
 
	        servivce_etat.create_etat(pk_etat, dateformat ,"", 1234, "127.0.0.1");
	        
	        
 
	}

}
