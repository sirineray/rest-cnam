package com.javasampleapproach.springrest.mysql.Service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.springrest.mysql.model.huissier;
import com.javasampleapproach.springrest.mysql.model.huissier_pk;
import com.javasampleapproach.springrest.mysql.model.tribunal;
import com.javasampleapproach.springrest.mysql.repo.HuisRepo;

@Service
public class huiss_service {

	
		@Autowired
	 HuisRepo huiss_repo  ;
	
	public Optional<huissier> getHuiss(huissier_pk pk)
	{
		
		return huiss_repo.findById(pk);		
		
	}
	
	public void addhuissier(huissier huissier) {
		
		huiss_repo.save(huissier);
	}
	
	@Transactional
	public void deleteTriunal(int code) {
		
		huiss_repo.deleteHuissier(code);
	}

	
	
}
