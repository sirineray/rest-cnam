package com.javasampleapproach.springrest.mysql.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javasampleapproach.springrest.mysql.model.LoginPk;
import com.javasampleapproach.springrest.mysql.model.login;
import com.javasampleapproach.springrest.mysql.repo.LoginRepo;

@Service
public class LoginService {

	@Autowired
	LoginRepo repo ; 
	
	public Optional<login> getById(LoginPk pk  )
	{
		
		return repo.findById(pk);
	}
	
	public Iterable<login> getAll()
	{
		return repo.findAll();
	}
}
