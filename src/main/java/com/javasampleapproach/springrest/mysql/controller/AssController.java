package com.javasampleapproach.springrest.mysql.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.springrest.mysql.Service.ass_service;
import com.javasampleapproach.springrest.mysql.model.Ass;
import com.javasampleapproach.springrest.mysql.model.AssCle;

import com.javasampleapproach.springrest.mysql.repo.AssRepo;
import com.javasampleapproach.springrest.mysql.repo.AssureServiceInter;

@CrossOrigin(origins = "http://localhost:4200")
@Controller
@RequestMapping("/connexion")

public class AssController {
 
	@Autowired
	 ass_service  service;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/get/{name}")
	@ResponseBody 
	public List<Ass> findBymatricule(@PathVariable String name ) {

	// AssCle	ass_cle = new AssCle(a,b);
	 
	  List<Ass> assure = service.getByNom(name);
	
	  return assure; 	 
	}

	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/SaisiDoss/{matricule}/{cle}")
	@ResponseBody 
	public Optional<Ass> findBymatricule(@PathVariable int matricule ,@PathVariable int cle ) {

	 AssCle	ass_cle = new AssCle(matricule,cle);
 
	  return service.getByPk(ass_cle);	
	  
	}

 
 
}
	
	
		



 
	
	
	
	
	

	
	

