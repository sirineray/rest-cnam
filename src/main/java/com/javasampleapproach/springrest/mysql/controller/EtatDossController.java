package com.javasampleapproach.springrest.mysql.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javasampleapproach.springrest.mysql.Service.EtatDoss_service;
import com.javasampleapproach.springrest.mysql.Service.huiss_service;
import com.javasampleapproach.springrest.mysql.model.EtatDoss;
import com.javasampleapproach.springrest.mysql.model.huissier;
import com.javasampleapproach.springrest.mysql.model.huissier_pk;

@CrossOrigin(origins = "http://localhost:4200")
@Controller
@RequestMapping("/etatdossier")
public class EtatDossController {
	
	@Autowired
	EtatDoss_service service ; 
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/get/{br}/{anne}/{num}")
	@ResponseBody 
	public int  findSituation(@PathVariable int br , @PathVariable int anne ,  @PathVariable int num )
	{
     return  service.getMaxsit(anne, br, num) ;
       
    }
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/instance/{br}/{sit}")
	@ResponseBody 
	public List <EtatDoss>  ListeDossiers(@PathVariable int br , @PathVariable int sit ) 
	{
     return  service.instance(br, sit);
       
    }
}
	


