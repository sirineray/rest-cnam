package com.javasampleapproach.springrest.mysql.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.springrest.mysql.Service.Dossjug_service;
import com.javasampleapproach.springrest.mysql.model.CreationDossier;
import com.javasampleapproach.springrest.mysql.model.DossierJugement;
import com.javasampleapproach.springrest.mysql.model.Jugement_PK;
import com.javasampleapproach.springrest.mysql.repo.DossjugRepo;

/*
 * 
@CrossOrigin(origins = "http://localhost:4200")
*/
@RestController
@RequestMapping("/Dossjug")
@CrossOrigin(origins = "http://localhost:4200")

public class DossjugController {
	
	
	@Autowired
	Dossjug_service  service ;
	
	@Autowired
	DossjugRepo dossierJugRepo;
	

	//@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/getmaxref/{anne}/{br}")
	@ResponseBody 
	public  int getMaxRef(@PathVariable int anne , @PathVariable int br)
	{
   return  service.getAll(2019 , 99 );
    }
	
	
	
	//@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/get/{codeBR}/{numDoss}/{anneDoss}")
	@ResponseBody 
	public Optional<DossierJugement> findByDossjug(@PathVariable int codeBR , @PathVariable int numDoss, @PathVariable int  anneDoss)
	{

		Jugement_PK id_Dossjug = new  Jugement_PK(codeBR, numDoss, anneDoss);
		
		return  service.getDossjug(id_Dossjug);
}
	

	//@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/saveDossier/{code}/{cle}/{tri_code}/{matricule}/{matriculeAssure}/{dateAt}/{heureAt}/{actJugement}/{dateJugement}/{numJugement}/{empCode}/{empCle}")
	@ResponseBody 
	public String saveDossier( 
			@PathVariable int code,  
			@PathVariable int cle, 
			@PathVariable int tri_code, @PathVariable int matricule,
			@PathVariable  int matriculeAssure, @PathVariable  String dateAt,
			@PathVariable int heureAt,@PathVariable  String actJugement,
			@PathVariable String dateJugement,@PathVariable int numJugement,
			@PathVariable int empCode,@PathVariable int empCle)
	{
		
		int num_dossier  = service.getAll(2019 , 99 );  
		 service.SaveDossier( num_dossier ,code,   cle,   tri_code,   matricule,   matriculeAssure,   dateAt,
				  heureAt,   actJugement,   dateJugement,   numJugement,
				  empCode,   empCle);
		 return ("ok ") ;
		
	}
	
	//@CrossOrigin(origins = "http://localhost:4200")
	
	/*@PostMapping(path="/create")	
	public DossierJugement enregistrer(@RequestBody DossierJugement dossier)
	{
		 // save data in service 
		 return this.dossierJugRepo.save(dossier); 
		
	}*/
	
	/* @PostMapping(path="/create")
	public  CreationDossier enregistrer(@RequestBody DossierJugement dossier)

	{
		
		//@RequestBody CreationDossier dossier
		 System.out.println("variables : ");
		 //+dossier.getHuis_code() + dossier.getDate_juge_string());
		//this.service.SaveDossier(dossier.);
		 return null;
		
	} */
	
	/*@PostMapping("/testpost")
	public String testPost(@RequestBody String ch) {
		System.out.println("ok"+ch);
		return "hello";
	}*/
	
	
 
	@RequestMapping( "/create")
	@ResponseBody 
	public CreationDossier enregistrer(@RequestBody CreationDossier dossier)
	{
		   
		 
		 int num_dossier  = service.getAll(2019 , 99 );  
		 service.SaveDossier( num_dossier ,dossier.getHuis_code(),   dossier.getHuis_cle(), 
				 dossier.getTrub(),   dossier.getCle(), dossier.getMatr(), dossier.getDate_accident(),
				  dossier.getHeure(),   dossier.getActjug(),   dossier.getDate_juge_string()
				  ,   dossier.getN_jug(),
				  dossier.getEmpMat(),   dossier.getEmpCle()
				  ); 
		  System.out.println("creation dossier");
		 return (dossier) ;
		
	} 
	 
	
}
