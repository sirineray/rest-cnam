package com.javasampleapproach.springrest.mysql.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javasampleapproach.springrest.mysql.Service.LiquidDoss_service;
import com.javasampleapproach.springrest.mysql.model.LiquidDoss;
import com.javasampleapproach.springrest.mysql.model.LiquidDoss_PK;
@CrossOrigin(origins = "http://localhost:4200")

@Controller
@RequestMapping("/LiquidDoss")

public class LiquidationDossController {


	@Autowired
	LiquidDoss_service  service ;
	
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/get/{idLiquidation}/{anneLiqu}")
	@ResponseBody 
	public Optional<LiquidDoss> findByLiquidDoss(@PathVariable int idLiquidation , @PathVariable int anneLiqu)
	{
	LiquidDoss_PK id_LiquidDoss = new  LiquidDoss_PK(idLiquidation, anneLiqu);
		
		return  service.getLiquidDoss(id_LiquidDoss);
}

}	
	