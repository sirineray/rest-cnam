package com.javasampleapproach.springrest.mysql.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javasampleapproach.springrest.mysql.Service.TribunalService;
import com.javasampleapproach.springrest.mysql.model.tribunal;
import com.javasampleapproach.springrest.mysql.repo.tribunalRepo;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/tribunal")
public class tribunalController {

	@Autowired
	TribunalService tribunalService;

	@Autowired
	tribunalRepo tribunalRepo;

	// public List<tribunal> findById( ) {}
	
	@GetMapping(value = "/getTribunal")
	public List<tribunal> getAll() {
		return tribunalRepo.findAll();
	}
	
	@PostMapping(value = "/addTribunal")
	public List<tribunal> addTribunal(@RequestBody tribunal tribunal) {

		tribunalService.addTribunal(tribunal);
		return tribunalRepo.findAll();

	}

	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> deleteCustomer(@PathVariable("id") int id) {
		System.out.println("Delete Customer with ID = " + id + "...");

		tribunalService.deleteTriunal(id);

		return new ResponseEntity<>("Customer has been deleted!", HttpStatus.OK);
	}

}
