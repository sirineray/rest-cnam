package com.javasampleapproach.springrest.mysql.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javasampleapproach.springrest.mysql.Service.huiss_service;
import com.javasampleapproach.springrest.mysql.model.Ass;
import com.javasampleapproach.springrest.mysql.model.huissier;
import com.javasampleapproach.springrest.mysql.model.huissier_pk;
import com.javasampleapproach.springrest.mysql.model.tribunal;
import com.javasampleapproach.springrest.mysql.repo.HuisRepo;

@CrossOrigin(origins = "http://localhost:4200")
@Controller
@RequestMapping("/huiss")
public class HuissController {
	
	@Autowired
	huiss_service  service ;
	
	@Autowired
	HuisRepo  repo ;
	
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/get/{code}/{cle}")
	@ResponseBody 
	public Optional<huissier> findByHuiss(@PathVariable int code , @PathVariable int cle)
	{

		huissier_pk id_huiss = new  huissier_pk(code, cle);
		
		return  service.getHuiss(id_huiss);
}
	
	@PostMapping(value = "/addHuissier")
	public List<huissier> addTribunal(@RequestBody huissier huissier) {

		service.addhuissier(huissier);
		return (List<com.javasampleapproach.springrest.mysql.model.huissier>) repo.findAll();

	}

	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> deleteCustomer(@PathVariable("id") int id) {
		System.out.println("Delete Customer with ID = " + id + "...");

		service.deleteTriunal(id);

		return new ResponseEntity<>("Customer has been deleted!", HttpStatus.OK);
	}
}